#!/bin/bash

GITLABTOKEN=$1
CI_COMMIT_TAG=$2
CI_PROJECT_ID=$3

curl --header "Content-Type: application/json" --header "PRIVATE-TOKEN: $GITLABTOKEN" --data '{ "name": "logol client release", "tag_name": "'"$CI_COMMIT_TAG"'", "description": "logol client", "assets": { "links": [{ "name": "logol2-client-'"$CI_COMMIT_TAG.linux.amd64"'", "url": "https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e/osallou_artifacts/org.irisa.genouest.logol2/logol2-client/'"$CI_COMMIT_TAG"'/logol2-client-'"$CI_COMMIT_TAG"'.linux.amd64" }, { "name": "logol2-client-'"$CI_COMMIT_TAG"'.darwin.amd64", "url": "https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e/osallou_artifacts/org.irisa.genouest.logol2/logol2-client/'"$CI_COMMIT_TAG"'/logol2-client-'"$CI_COMMIT_TAG"'.darwin.amd64" }, { "name": "logol2-client-'"$CI_COMMIT_TAG"'.windows.amd64.exe", "url": "https://genostack-api-swift.genouest.org/v1/AUTH_db3183b479c9476b8d3c4948d27f247e/osallou_artifacts/org.irisa.genouest.logol2/logol2-client/'"$CI_COMMIT_TAG"'/logol2-client-'"$CI_COMMIT_TAG"'.windows.amd64.exe" }] } }' --request POST https://gitlab.inria.fr/api/v4/projects/${CI_PROJECT_ID}/releases

