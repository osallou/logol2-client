package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"

	"text/template"

	logol "gitlab.inria.fr/osallou/logol2-lib"
	"go.uber.org/zap"
)

var logger = logol.GetLogger("logol.client")

// Version current version of software
var Version string

// Buildtime time of the build
var Buildtime string

// Sha1ver represents Git sha
var Sha1ver string

func main() {
	outFile := flag.String("output", "", "Output file (default: same as grammar file")
	grammarFile := flag.String("grammar", "", "Path to grammar yml file")
	check := flag.Bool("check", false, "only check grammar")
	version := flag.Bool("version", false, "show version and build info")
	module := flag.String("module", "", "Generate grammar as a module instead of an executable using provided module name")
	flag.Parse()

	if *version {
		fmt.Printf("Version: %s, Build time: %s, SHA: %s\n", Version, Buildtime, Sha1ver)
		return
	}

	if *grammarFile == "" {
		fmt.Println("Missing grammar file in command line")
		return
	}

	outputPath := strings.TrimSuffix(*grammarFile, path.Ext(*grammarFile)) + ".go"
	if *outFile != "" {
		outputPath = path.Join(*outFile, strings.TrimSuffix(filepath.Base(*grammarFile), path.Ext(*grammarFile))+".go")
	}

	grammarpath := *grammarFile
	g, gerr := ioutil.ReadFile(grammarpath)
	if gerr != nil {
		panic("Cannot read grammar file")
	}
	grammar, err := logol.LoadGrammar([]byte(g))
	if err != nil {
		logger.Error("Failed to load grammar", zap.Error(err))
		return
	}

	if *check {
		ok := grammar.Check()
		if ok {
			fmt.Printf("Grammar looks fine...\n")
		} else {
			os.Exit(1)
		}
		return
	}

	if !grammar.Check() {
		logger.Error("Invalid grammar")
		os.Exit(1)
	}

	grammarJSON, grammarJSONErr := json.Marshal(grammar)
	if grammarJSONErr != nil {
		panic("Failed to decode grammar")
	}

	data := "package main"
	if *module != "" {
		data = "package " + *module
	}
	data += `
import (
	"encoding/json"
	"flag"
	"fmt"
	// "io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"time"
	logol "gitlab.inria.fr/osallou/logol2-lib"
	"go.uber.org/zap"
	yaml "gopkg.in/yaml.v2"

`

	declaredPlugins := make(map[string]string)
	for _, plugin := range grammar.Plugins {
		declaredPlugins[plugin.Name] = plugin.Package
	}
	plugins := make(map[string]string)
	for _, model := range grammar.Models {

		for _, varDef := range model.Vars {
			if varDef.Plugin.Search.Name != "" {
				plugins[varDef.Plugin.Search.Name] = ""
			}
			for _, plugin := range varDef.Plugin.Checks {
				plugins[plugin.Name] = ""
			}

			for pname := range declaredPlugins {
				if strings.Contains(varDef.Model.Name, pname) {
					plugins[pname] = ""
					break
				}
			}
		}

		for pname := range declaredPlugins {
			found := false
			for _, meta := range model.Meta {
				if strings.Contains(meta, pname) {
					found = true
					break
				}
			}
			if found {
				plugins[pname] = ""
			}
		}

	}
	for pname := range declaredPlugins {
		found := false
		for _, meta := range grammar.Meta {
			if strings.Contains(meta, pname) {
				found = true
				break
			}
		}
		if found {
			plugins[pname] = ""
		}
	}
	for k := range plugins {
		if plugin, ok := declaredPlugins[k]; ok {
			data += fmt.Sprintf("    %s \"%s\"", k, plugin)
		} else {
			logger.Error("plugin not defined", zap.String("plugin", k))
			panic("plugin is not defined")
		}
	}

	data += `
)

type Results struct {
	LogolVersion string
	GrammarVersion string
	Matches map[string]uint64
	Sequence string
	Stats map[string]*logol.Stat
	Files []string
}

var logger = logol.GetLogger("logol.client")
	`
	for modelName, modelData := range grammar.Models {
		isMain := false
		for _, m := range grammar.Run {
			if modelName == m.Model {
				isMain = true
				break
			}
		}
		if *module != "" {
			isMain = false
		}
		modelTemplate, err := logol.GenerateModel(modelName, modelData, isMain)
		if err != nil {
			logger.Error("Failed to generate code", zap.Error(err))
			return
		}
		data += modelTemplate + "\n"

	}

	type Node struct {
		ID    string `json:"id"`
		Label string `json:"label"`
		Size  int    `json:"size"`
		X     int    `json:"x"`
		Y     int    `json:"y"`
	}
	type Edge struct {
		ID     string `json:"id"`
		Source string `json:"source"`
		Target string `json:"target"`
		Type   string `json:"type"`
	}
	type Graph struct {
		Nodes []Node `json:"nodes"`
		Edges []Edge `json:"edges"`
	}
	graph := Graph{
		Nodes: make([]Node, 0),
		Edges: make([]Edge, 0),
	}
	modY := 10
	modX := 10
	varX := 20
	for modName, modInfo := range grammar.Models {
		info := ""
		if modInfo.Comment != "" {
			info = fmt.Sprintf("(%s)", modInfo.Comment)
		}
		graph.Nodes = append(graph.Nodes, Node{
			ID:    modName,
			Label: modName + info,
			X:     modX,
			Y:     modY,
			Size:  1,
		})
		modY += 10
		for _, next := range modInfo.Start {
			graph.Edges = append(graph.Edges, Edge{
				ID:     modName + "_" + next,
				Source: modName,
				Target: modName + "-" + next,
				Type:   "arrow",
			})
		}
		for varName, varInfo := range modInfo.Vars {
			extra := ""
			if varInfo.Model.Name != "" {
				extra = fmt.Sprintf("[->%s]", varInfo.Model.Name)
			}
			info := ""
			if varInfo.Comment != "" {
				info = fmt.Sprintf("(%s)", varInfo.Comment)
			}
			graph.Nodes = append(graph.Nodes, Node{
				ID:    modName + "-" + varName,
				Label: modName + "-" + varName + info + extra,
				X:     varX,
				Y:     modY,
				Size:  1,
			})
			varX += 10
			for _, next := range varInfo.Next {
				graph.Edges = append(graph.Edges, Edge{
					ID:     modName + "_" + next,
					Source: modName + "-" + varName,
					Target: modName + "-" + next,
					Type:   "arrow",
				})
			}

		}
	}

	for index, meta := range grammar.Meta {
		data += fmt.Sprintf(`
func meta%d(ctx logol.Context) (bool, error) {
			%s
}
`, index, logol.EvalCompareFunction(meta))
	}

	for modelName, model := range grammar.Models {
		for index, meta := range model.Meta {
			data += fmt.Sprintf(`
func %s_%s_meta%d(ctx logol.Context) (bool, error) {
			%s
}
`, modelName, modelName, index, logol.EvalCompareFunction(meta))
		}
	}

	data += `
// OUT_EXTENSION defines output file extension name
const OUT_EXTENSION = ".json"

`
	data += fmt.Sprintf("var grammarJSON = `%s`\n", grammarJSON)

	// init models
	initModels := "func initModels() map[string]interface{} {\n"
	initModels += "    	models := make(map[string]interface{})\n"

	for pname := range declaredPlugins {
		for _, modelData := range grammar.Models {
			for _, varDef := range modelData.Vars {
				if strings.Contains(varDef.Model.Name, pname) {
					initModels += fmt.Sprintf("models[\"%s\"] = %s\n", varDef.Model.Name, varDef.Model.Name)
					initModels += fmt.Sprintf("models[\"%sParams\"] = %sParams\n", varDef.Model.Name, varDef.Model.Name)

				}
			}
		}
	}

	//look over model/vars to add functions in logol.models on SizeMin, SizeMax, etc.
	funcs := []string{"SizeMin", "SizeMax", "StartMin", "StartMax", "EndMin", "EndMax", "SpacerMin", "SpacerMax", "CostMin", "CostMax", "DistanceMin", "DistanceMax", "RepeatMin", "RepeatMax", "RepeatSpacerMin", "RepeatSpacerMax", "RepeatOverlapMin", "RepeatOverlapMax", "OverlapMin", "OverlapMax"}
	for modelName, model := range grammar.Models {
		for mindex := range model.Meta {
			funcName := fmt.Sprintf("%s_%s_meta%d", modelName, modelName, mindex)
			initModels += fmt.Sprintf(`
		models["%s"]=%s
			`, funcName, funcName)
		}
		for variable := range model.Vars {
			for _, fn := range funcs {
				funcName := modelName + "_" + variable + fn
				initModels += fmt.Sprintf(`
		models["%s"]=%s
			`, funcName, funcName)
			}
		}
	}

	for index := range grammar.Meta {
		initModels += fmt.Sprintf(`
        models["meta%d"]=meta%d
`, index, index)
	}

	for modelName := range grammar.Models {
		initModels += fmt.Sprintf(`
		models["%s"] = %s		
`, modelName, modelName)
	}
	initModels += "return models\n"

	initModels += "}\n"
	data += initModels
	// end init models

	moduleCall := `
// Call executes module entrypoint
func Call(chin chan logol.Match, chout chan logol.Match, ctx logol.Context) {
	newCtx := ctx
	newCtx.Models = initModels()
	var grammar logol.Grammar
	json.Unmarshal([]byte(grammarJSON), &grammar)
	newCtx.Grammar = &grammar
	// Set local morphisms
	morphingRules := logol.NewMorphisms(ctx.Kind)
	for morphName, morphRules := range grammar.Morphisms {
		morphingRules.AddMorphism(morphName, morphRules.Morph)
	}
	newCtx.Morphisms = morphingRules
	{{.Model}}(chin, chout, newCtx)
}

// CallParams returns the list of parameters for entrypoint
func CallParams() []string {
	modParams := []string {
		{{ range $mod := .Param }}
		"{{$mod}}",
		{{ end }}
	}
	return modParams
}

`
	if grammar.Run[0].Model != "" {
		var moduleCode bytes.Buffer
		moduleTemplate, _ := template.New("module").Parse(moduleCall)
		err = moduleTemplate.Execute(&moduleCode, grammar.Run[0])
		data += moduleCode.String()
	}

	data += `func main() {
		kind := flag.String("kind", "dna", "type of sequence [dna|rna|protein]")
		outDir := flag.String("dir", "", "Output directory (default: same as grammar file)")
		sequenceInput := flag.String("sequence", "", "Path to fasta sequence file")
		jsonOutput := flag.Bool("json", false, "display result output in JSON format")
		fastaOutput := flag.Bool("fasta", false, "generate result in Fasta format")
		version := flag.Bool("version", false, "show version")
		grammarOut := flag.Bool("grammar", false, "show grammar and exit")
		zipOut := flag.Bool("zip", false, "zip output files to create an archive")
		flag.Parse()
		`
	data += "        LogolVersion := \"" + Version + "\"\n"
	if grammar.Version == "" {
		grammar.Version = "0.0"
	}
	data += "        GrammarVersion := \"" + grammar.Version + "\""
	graphJSON, _ := json.Marshal(graph)
	data += fmt.Sprintf("\n    graphJSON := `%s`\n", graphJSON)
	data += fmt.Sprintf("\n    grammarJSON = `%s`\n", grammarJSON)
	data += `

	var kindSequence int
	if *kind == "dna" {
		kindSequence = logol.DNA
	} else if *kind == "rna" {
		kindSequence = logol.RNA
	} else if *kind == "protein" {
		kindSequence = logol.PROTEIN
	} else {
		kindSequence = logol.DNA
	}

	if *version {
		fmt.Printf("Logol version: %s\n", LogolVersion)
		fmt.Printf("Grammar version: %s\n", GrammarVersion)
		os.Exit(0)
	}

	var grammar logol.Grammar
	err := json.Unmarshal([]byte(grammarJSON), &grammar)
	if err != nil {
		logger.Error("Failed to load grammar", zap.Error(err))
		return
	}
	if *grammarOut {
		out, _ := yaml.Marshal(grammar)
		fmt.Printf("%s\n", out)
		return
	}

	if os.Getenv("LOGOL_WEB") != "" {
		logol.ServeProgress(graphJSON)
		return
	}

	if *sequenceInput == "" {
		fmt.Println("sequence argument missing")
		return
	}
	outResultDir := *outDir
	if outResultDir == "" {
		outResultDir = filepath.Dir(*sequenceInput)
	}
	fastaFiles, fastaErr := logol.Fasta2logol(*sequenceInput, *outDir)
	if fastaErr != nil {
		fmt.Printf("Failed to parse fasta files %+v", fastaErr)
		return
	}
	defer logol.CleanupFiles(fastaFiles)

	matchRes := make(map[string]uint64, 0)

 	logger.Debug("Search in sequence", zap.String("sequence",*sequenceInput))

	go logol.ServeProgress(graphJSON)
	if len(os.Args) < 2 {
		panic("No sequence file given as input")
	}

	for _, seqInfo := range fastaFiles {
		seq := seqInfo.Path
		f, errf := os.Create(seq + OUT_EXTENSION)
		if errf != nil {
			panic("Cannot write to result file")
		}
		logger.Debug("Search in sequence", zap.String("sequence", seq))
		sequence := logol.NewSequence(seq)
		lru := logol.NewSequenceLru(sequence)
		ctx := logol.Context {
			Kind: *kind,
			Position: 0,
			Spacer: false,
			// Match: nil,
			Progress: make(chan string),
			Grammar: &grammar,
			SequenceInput: seq,
			Sequence: &sequence,
			SequenceLru: &lru,
		}
		ctx.CassieHandler = logol.NewCassieManager(seq, kindSequence)
		go logol.TrackProgress(ctx.Progress)

`
	modelsCall := make([]logol.ModelCall, 0)
	mainParams := make(map[string]bool)
	for _, run := range grammar.Run {
		for _, param := range run.Param {
			if _, ok := mainParams[param]; !ok {
				//data += fmt.Sprintf("    var %s *logol.Variable\n", param)
				mainParams[param] = true
			}
		}
		modelsCall = append(modelsCall, logol.ModelCall{
			Fn:     run.Model,
			Params: run.Param,
		})

	}

	data += "    	ctx.Models = make(map[string]interface{})\n"

	data += "       ctx.Models = initModels()\n"
	/*
			//look over model/vars to add functions in logol.models on SizeMin, SizeMax, etc.
			funcs := []string{"SizeMin", "SizeMax", "StartMin", "StartMax", "EndMin", "EndMax", "SpacerMin", "SpacerMax", "CostMin", "CostMax", "DistanceMin", "DistanceMax", "RepeatMin", "RepeatMax", "RepeatSpacerMin", "RepeatSpacerMax", "RepeatOverlapMin", "RepeatOverlapMax", "OverlapMin", "OverlapMax"}
			for modelName, model := range grammar.Models {
				for mindex := range model.Meta {
					funcName := fmt.Sprintf("%s_%s_meta%d", modelName, modelName, mindex)
					data += fmt.Sprintf(`
				ctx.Models["%s"]=%s
					`, funcName, funcName)
				}
				for variable := range model.Vars {
					for _, fn := range funcs {
						funcName := modelName + "_" + variable + fn
						data += fmt.Sprintf(`
				ctx.Models["%s"]=%s
					`, funcName, funcName)
					}
				}
			}

			for index := range grammar.Meta {
				data += fmt.Sprintf(`
		        ctx.Models["meta%d"]=meta%d
		`, index, index)
			}

			for modelName := range grammar.Models {
				data += fmt.Sprintf(`
				ctx.Models["%s"] = %s
		`, modelName, modelName)
			}

	*/

	data += `
	morphingRules := logol.NewMorphisms(*kind)

	for morphName, morphRules := range grammar.Morphisms {
		morphingRules.AddMorphism(morphName, morphRules.Morph)
	}

	ctx.Morphisms = morphingRules
`

	search := `
		{{ $nbModels := .NbModels }}
		{{ $lastModelIndex := .LastModelIndex }}
		{{ $prevModels := .PrevModels }}
		{{ $models := .Models }}

		matchStart := logol.Match{
			Value:  "init",
			Match:  make([]logol.Match, 0),
			Vars:   make(map[string]logol.Match),
			Spacer: true,
		}
		matchesToEvaluate := make([]logol.Match, 1)
		matchesToEvaluate[0] = matchStart
		totalMatches := 0
		for len(matchesToEvaluate) > 0 {
			matchStart = matchesToEvaluate[0]
			matchesToEvaluate = matchesToEvaluate[1:]
			// Do search
			chin := make(chan logol.Match)
			{{ $inputChan := "chin" }}
			{{ $outputChan := "chout"}}
		
		{{range $index, $model := .Models}}
			{{ $model.Fn }}Chan := make(chan logol.Match)
			{{ if eq $index 0 }}
			go {{ $model.Fn }}(chin, {{ $model.Fn }}Chan, ctx)
			{{ else }}
			go {{ $model.Fn }}({{ (index $prevModels $index).Fn}}Chan, {{ $model.Fn }}Chan, ctx)
			{{ end }}
		{{end}}
			
			chin <- matchStart
			close(chin)
			chout := {{ (index .Models $lastModelIndex).Fn }}Chan

			for m := range chout {
				newCtx := ctx
				newCtx.Vars = m.Vars
				result := logol.MergeMatches(m, newCtx, logol.Variable{})
				if result.Defined {
					var resJSON []byte
					if len(grammar.Run) > 0 {
						refactored := logol.RefactorModels(newCtx, grammar, result)
						if refactored == nil {
							logger.Debug("refactored models do not match meta controls")
							continue
						}
						results := logol.Result{
							Fasta: seqInfo,
							Matches: refactored,
						}
						resJSON, _ = json.Marshal(results)
					} else {
						refactored := make([]logol.Match, 1)
						refactored[0] = result
						results := logol.Result{
							Fasta: seqInfo,
							Matches: refactored,
						}
						resJSON, _ = json.Marshal(results)
					}
					logger.Debug("got result", zap.Any("result", result))
					totalMatches++
					f.Write(resJSON)
					f.WriteString("\n")
				} else {
					logol.GetUndef(&result)
					if ! result.PrevFoundMatch[0].Defined {
						result.Position = 0
						result.Spacer = true
					} else {
						result.Spacer = false
					}
					matchesToEvaluate = append(matchesToEvaluate, result)
					//resJSON, _ := json.Marshal(result)
					logger.Debug("undefined", zap.Any("result", result))
				}

			}
			nbUndef := len(matchesToEvaluate)
			if nbUndef > 0 {
				logger.Debug("Undefined matches to reevaluate", zap.Int("count", nbUndef))
			}
		}

		matchRes[seq] = uint64(totalMatches)
		close(ctx.Progress)
		if !*jsonOutput {
			logger.Debug("Stats", zap.Any("stats", logol.GetProgress()))
			fmt.Printf("Number of match: %d\n", totalMatches)
			fmt.Println("Result files:")
			for index, seqInfo := range fastaFiles {
				seq := seqInfo.Path
				fmt.Printf("* %s\tseqID:%d\n", seq + OUT_EXTENSION, index)
			}
		}
		f.Close()

		if *fastaOutput {
			logol.Res2Fasta(seq, seq + OUT_EXTENSION, "")
		}

		os.Remove(seq)

	}

	resultFiles := make([]string, 0)
	for _, seqInfo := range fastaFiles {
		seq := seqInfo.Path
		resultFiles = append(resultFiles, seq + OUT_EXTENSION)
	}
	if *zipOut {
		zipOutFile, zipErr := logol.ZipFiles(outResultDir, resultFiles)
		if zipErr != nil {
			fmt.Printf("Failed to create result file archive: %s\n", zipErr)
		} else {
			for _, seq := range resultFiles {
				os.Remove(seq)
			}
			resultFiles := make([]string, 1)
			resultFiles[0]= zipOutFile
		}
	}

	if *jsonOutput {
		res := Results {
			LogolVersion: LogolVersion,
			GrammarVersion: GrammarVersion,
			Matches: matchRes,
			Sequence: *sequenceInput,
			Stats: logol.GetProgress(),
			Files: resultFiles,
		}
		jsonRes, _ := json.Marshal(res)
		fmt.Println(string(jsonRes))
	}

`
	searchTemplate, err := template.New("search").Parse(search)
	if err != nil {
		panic(err)
	}

	type SearchData struct {
		Models         []logol.ModelCall
		PrevModels     []logol.ModelCall
		NbModels       int
		LastModelIndex int
	}
	prevModels := make([]logol.ModelCall, len(modelsCall)+1)
	prevModels[0] = logol.ModelCall{}
	for index, m := range modelsCall {
		prevModels[index+1] = m
	}
	searchData := SearchData{
		Models:         modelsCall,
		PrevModels:     prevModels,
		NbModels:       len(modelsCall),
		LastModelIndex: len(modelsCall) - 1,
	}
	var searchCode bytes.Buffer
	err = searchTemplate.Execute(&searchCode, searchData)
	data += searchCode.String()
	data += "}\n"

	// outputPath := filepath.Join("testdata", "run.go")
	errWrite := ioutil.WriteFile(outputPath, []byte(data), 0644)
	if errWrite != nil {
		logger.Error("Failed to write grammar file", zap.Error(errWrite))
		return
	}

}
