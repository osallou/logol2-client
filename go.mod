module gitlab.inria.fr/osallou/logol2-client

go 1.13

require (
	gitlab.inria.fr/osallou/logol2-lib v0.8.4
	go.uber.org/zap v1.13.0
)
