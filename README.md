# Logol 2

Developed at IRISA, a French academic research institute, Logol is a pattern matching grammar language and tool to search a pattern in a sequence (nucleic or proteic).
Pattern is described in Yaml format as a workflow where workflow steps define what is expected in sequence.
Each step can specify the allowed errors, the number of repetitions or morphisms, etc.

Result file give final matches with the details of each step. Results can also be exported to Fasta format.

Features:

* Exact, approximate, local and remote search
* Hamming and distance errors support
* Unlimited variables use
* Transformations/morphisms (reverse, complement, custom)
* Contraints: position, size, length,...
* Custom plugins
* And much more!

Logol2 is a rewrite of logol in Go language.

Status: **in development**

## License

Apache 2.0

## Requirements

* golang compiler

## Usage

    version=0.1
    go build -ldflags "-X main.Buildtime=`date -u '+%Y-%m-%d_%I:%M:%S%p'` -X main.Sha1ver=`git rev-parse HEAD` -X main.Version=$version"
    ./logol2 -h

## Test

    go run logolClient.go --grammar testdata/basic.yml --output ./testdata
    cd testdata
    go mod init basic
    go mod tidy
    # Wanna a try?
    LOGOL_DEBUG=0 go run basic.go  --sequence sequence.fasta

To build redistribuable binaries and avoid Go compiler afterwards:

    GOOS=linux GOARCH=amd64 go build -o basic.linux.amd64 basic.go
    GOOS=darwin GOARCH=amd64 go build -o basic.darwin.amd64 basic.go
    GOOS=windows GOARCH=amd64 go build -o basic.windows.amd64.exe basic.go

## Web UI

It is possible to launch generated code with an UI to check for model

    LOGOL_WEB=1 LOGOL_LISTEN=:8080 go run run.go

Then go to [http://localhost:8080/model](http://localhost:8080/model)

LOGOL_WEB=X will "block" the process until Ctrl-C

During search, if LOGOL_LISTEN is set, all urls are available:

* /model : graphical view of the model
* /progress: JSON stats on number of matches in each component

## Env variables

* LOGOL_DEBUG = 1 activate debug level logging
* LOGOL_SLOW = X pause X seconds between each search

## TODO

* Use zap log library and add model/var as params in logs [done}
* Cost & distance [done, to be tested]
* morphism (with reverse) [done, to be tested]
* overlap [done, to be tested]
* test spacers (specially after unknown) [done, to be tested]
* spacers and overlap in repeats [done, to be tested]
* manage case repeat 0,X (zero repeat allowed, we have no min but we have a max)
* can we enhance search using known constraints
* use [cassiopee lib](https://gitlab.inria.fr/osallou/suffixsearch) for exact/approximate search with spacer [done]
* add arg cmd line for generated client (max result, seq)
* manage seqs in fasta format, if multiple seqs, generate seq file for each and loop (1 result file per seq) [done]
