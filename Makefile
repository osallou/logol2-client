VERSION := $(shell git rev-parse --short HEAD)
BUILDTIME := $(shell date -u '+%Y-%m-%dT%H:%M:%SZ')

GOLDFLAGS += -X main.Version=$(VERSION)
GOLDFLAGS += -X main.Buildtime=$(BUILDTIME)
GOFLAGS = -ldflags "$(GOLDFLAGS)"

run: build

lint:
	go vet ./...
test:
	go test ./...

build:
	GOOS=linux GOARCH=amd64 go build -o logol2.linux.amd64 $(GOFLAGS) . ; \
        GOOS=darwin GOARCH=amd64 go build -o logol2.darwin.amd64 $(GOFLAGS) . ; \
        GOOS=windows GOARCH=amd64 go build  -o logol2.windows.amd64.exe $(GOFLAGS) .


