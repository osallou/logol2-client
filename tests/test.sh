#!/bin/bash

set -e

WORKDIR=$PWD
TESTDIR=$PWD/tests

FILES=$TESTDIR/*.yml
for f in $FILES
do
  cd $WORKDIR
  gotmpdir=$(mktemp -d -t ci-XXXXXXXXXX)
  echo "Processing $f file in $gotmpdir..."
  go run logolClient.go --grammar $f --check
  go run logolClient.go --grammar $f  --output $gotmpdir
  if [ -e $TESTDIR/$f.fasta ]; then
    cp $TESTDIR/$f.fasta $gotmpdir/sequence.fasta
  else
    cp $TESTDIR/sequence.fasta $gotmpdir/
  fi
  cd $gotmpdir
  go mod init test
  go mod tidy
  go build -o test
  echo "Run grammar"
  RES=`timeout 20 ./test --sequence sequence.fasta --json | jq -r .Files[0]`
  echo "Count results..."
  COUNT=`cat $RES | jq '.Matches | length' | wc -l`
  if [ "$COUNT" == 0 ]; then
      echo "$f: No result!"
      exit 1
  else
      echo "$f: Got $COUNT matches"
  fi
  rm -rf $gotmpdir
done
