# Getting started

## What is logol

Developed at IRISA, a French academic research institute, Logol is a pattern matching grammar language and tool to search a pattern in a sequence (nucleic or proteic).
Pattern is described in Yaml format as a workflow where workflow steps define what is expected in sequence.
Each step can specify the allowed errors, the number of repetitions or morphisms, etc.

Result file give final matches with the details of each step. Results can also be exported to Fasta format.

Features:

* Exact, approximate, local and remote search
* Hamming and distance errors support
* Unlimited variables use
* Transformations/morphisms (reverse, complement, custom)
* Contraints: position, size, length,...
* Custom plugins
* And much more!

## Grammar

Logol takes as input a grammar file, in YAML format.
With logol client, a *.go* file is created for the input grammar.
Next this file needs to be compiled (with the Go compiler). Grammar can be
compiled for linux, mac and windows.

Once compiled, you can run the resulting executable against a sequence to
get the search results. Executable being a static binary, you can copy it for
later usage but also share/distribute it. The same grammar will be used against
your different sequences.

## Structure

Let's take a very simple example to starts with:

    models:
        mod1:
            comment: 'mod1()'
            start:
                - var1
            vars:
                var1:
                    value: 'cccc'

    run:
        - model: mod1

At first, we defined a **run** entry.
In **run** we define the starting points of the search, i.e. the models to
call.
In this example, we want to call model *mod1*. A **run** always call one
(or more) **model**.
All models called in the **run** section will allow for a **spacer**.
An allowed spacer means that we will look for a match with the model but
it *may* at any position in the sequence.

A **model** is a kind of *function* call.
Models are defined in the **models** section.

**CAUTION**: always check you call an existing model, variable, etc.

We'll see later on more details about **model** definition, let's keep
it simple for the moment.

A **model** is defined by an optional *comment* and one or more starting
points. In above example, our model *mod1* starts with a single **variable**
var1.
Each **variable** is defined sequentially in the **vars** section of a
**model** but order is not important.

**variables** define what we want to search in input sequence. It can be a
string or a previously found variable (we'll see that later on).
In our case, we simply look for the string "cccc" with no allowed error.
To do so we use the *constraint* **value** followed by the string to find.
As our variable as no **next** defined, it means that we will end the search here.

If we execute this example, we will get the position in the sequence for all
matches with the string "cccc".

To execute, write above grammar in a file named example.yml

    logolClient --grammar example.yml --output /tmp
    cd /tmp
    go mod init example
    go mod tidy
    # Create a fasta sequence file in /tmp
    go run example.go --sequence sequence.fasta

Want to check what looks like your grammar?

     LOGOL_LISTEN=:8080 LOGOL_WEB=1 go run example.go
     # Open your navigator to http://localhost:8080/model

## Chaining and branching

### Chaining

If we want to search for "cccc" followed by "acgt", for example, we can declare
several variables and ask to find one after the other
(meaning "acgt" starts right after "cccc").

To do so we would write:

    vars:
        var1:
            value: 'cccc'
            next:
                - var2
        var2:
            value: 'acgt'

The **next** parameter specify one or more other variables to search for after
the current variable.
If var1 does not find any match, it will stop here. Else, for each match, it
will try find a match for the variables defined in **next**.

## Branching

In the same way, we may want to find after 'cccc', either 'acgt' **or** 'tgca'.
To do so, we simply set several values in the **next** section.

Example:

    vars:
        var1:
            value: 'cccc'
            next:
                - var2
                - var3
        var2:
            value: 'acgt'
        var3:
            value: 'cgta'

A model can also defined several starting points (a branch) in its **start** section.

## Join

It is still possible to join the branches again simply by setting the next section the same variable:

    vars:
        var1:
            value: 'cccc'
            next:
                - var2
                - var3
        var2:
            value: 'acgt'
            next:
                - var4
        var3:
            value: 'cgta'
            next:
                - var4
        var4:
            value: 'tttt'

We will search here for 'cccc', then 'acgt' OR 'cgt' , then 'tttt'

## Reusing variables

It can be useful to search for something, then trying to find the same later one,
possibly with other constraints.
To do so, it is possible to save a match.

    var10:
        comment: 'aa{?RAA}'
        value: 'aa'
        string_constraints:
          saveas: 'RAA'

With the *saveas* property, the result of the match will be saved in defined variable.
Not only content, but also all match properties (cost, size, distance, etc.)

A saved variable can be reused within the scope of the model where it was saved.
If variable needs to be reused in sub models, then it must be passed as a parameter.

To reuse a variable, simply call its property in constraints.

    var10:
        comment: 'aa{?1}'
        value: 'aa'
        string_constraints:
          saveas: 'R1'
        next:
          - var11
    var11:
        string_contraints:
            content: 'R1' # search for same content found in var10
        next:
         - var12
    var12:
        string_constraints:
          size:
            min: 'vars.R1.Len'
            max: 'vars.R1.Len' # search for anything with same length than R1.

In range constraints (min, max), one can reuse a variable with vars.*VarName*.*Property*,
with math operations: vars.R1.Len + 1 for example.

It is also possible to mix multiple variables: vars.R1.Len + vars.R2.Len

Available properties:

* Position: start position of the match
* Len: length of the match
* Cost: number of substitution
* Distance: number of insert/deletion
* RepeatIndex: number of repeats

## Constraints

### Start/End

Start and End string constraints expect a match to have a start (or end) position between specified range (inclusive).

    vars:
        ...
        var2:
            value: "acgg"
            string_constraints:
                Start:
                    Min: "4"
                    Max: "8"

### Content

Content constraint will search for a string matching a saved variable

In the following example, logol will search for a string matching variable content "R1".

    vars:
        ...
        var2:
            string_constraints:
                Content: "R1"

### Size

Size allows to check that a match is of a defined length range.

To do so, one must use string_constraints

    vars:
        ...
        var2:
            value: "acgg"
            string_constraints:
                Size:
                    Min: "4"
                    Max: "5"

In previous example, size constraint is of little use as we specify a value to search of length 4. But if we make use of variables, repeats, etc... you want look for a string of variable length. In such a case, the **Size** constraint will limit the results to the specified size.

If no value nor content constraint is specified, then it will match any string within size range.

### Morphisms

Morphisms modify the searched pattern with a defined morphism.
By default, for DNA/RNA, the *complement* morphism is already defined. Grammar can define addition morphism.

In complement with the *reverse* variable constraint, one can reverse a string and apply morphism.

    vars:
        ...
        var2:
            value: "acgg"
            string_constraints:
                Morphism: "complement"
                Reverse: true

will search for the reverse complement of "acgg"

To define your own morphism, add in grammar:

    morphisms:
        testmorphism:
            morph:
                a: "c"
                g: "c"

### Model

To ease the reuse of models or logically group some variables, it is possible to
define a model and to call it within an other model

      var5:
          comment: 'mod2(R1)'
          model:
            name: 'mod2'
            param:
              - 'R1'

In this example, var5 will call model *mod2* with a parameter.
Parameter may be an input or an output parameter.
If R1 is an input parameter, value will be given to mod2.
If R1 is an output parameter, then after mod2, R1 will be set with the resulting value.

Let's see a full example:

    models:
        mod2:
            param:
            - 'RAA'
            comment: 'mod2(RAA)'
            start:
            - var10
            vars:
            var10:
                comment: 'aa{?RAA}'
                value: 'aa'
                string_constraints:
                saveas: 'RAA'
        mod1:
            start:
                - var5
            vars:
                var5:
                    comment: 'mod2(R1)'
                    model:
                        name: 'mod2'
                        param:
                        - 'R1'
                        next: var6
                var6:
                    comment: '?R1'
                    string_constraints:
                    content: 'R1'
    run:
        - model: mod1

In this example, grammar calls mod1 and mod1.var5. var5 calls mod2 with param R1,
an ouput variable.
mod2 is defined with a variable calls "RAA"
mod2 starts with var10, searches for "aa" and save result in variable RAA.
Then mod2 exists and goes back to mod1.var5, RAA is mapped to R1.
Then mod1.var6 is executed and search for content defined by R1.

We could see this like below fake python code:

    def mod2():
    raa = search_for_var("aa")
    return raa

    def mod1():
    r1 = mod2()
    search_for_var(r1)

For an input variable it would be like:

    def mod2(raa):
    search_for_var(raa)

    def mod1():
    r1 = search_for_var()
    mod2(r1)

### Spacer

The spacer constraint tells logol that match may be found at a further position in sequence.
If a spacer is set, though min value may be zero, max must always be set.

Spacer values define the allowed range to find the variable from current position.

    vars:
        ...
        var2:
            value: "acgg"
            spacer: true
                Min: "0"
                Max: "100"

### Overlap

The overlap constraint tells logol that match may be found at a previous position in sequence,
overlapping with previous match.
If an overlap is set, min and max value must be defined.

    vars:
        ...
        var2:
            value: "acgg"
            overlap: true
                Min: "0"
                Max: "5"

### Repeat

With *repeat*, one can ask to find a variable, possibly multiple times.
If min is set, then max must be set too.

      var2:
        value: 'a'
        repeat:
          min: 0
          max: 3

The value "a" may be repeated up to 3 times.

*repeat* may also define spacer or overlap ranges. Those ranges will apply only
between repeats, not on first match.

### Errors

Grammar allows for search with errors i.e. substitutions (cost) and/or insert/deletions (distance).
For example, we can search for "acgg" against "acgt" with an allowed cost of 1.

    vars:
        ...
        var2:
            value: "acgg"
            struct_constraints:
                Cost:
                    Min: "0"
                    Max: "1"

For distance we set the struct_constraints Distance

    vars:
        ...
        var2:
            value: "acgg"
            struct_constraints:
                Distance:
                    Min: "0"
                    Max: "1"

## Negative constraints

Setting the *not* property on a variable will try to match the defined variable,
then take all matches that do not match previous search.

When a *not* is set, one **must** define a *size* string_constraint (will take all string
in size range and exclude those matching the definition)

## Plugin constraint

It is possible to extend logol with plugins (written in Go)

To define a plugin add in grammar:

    plugins:
      - name: "myplugin"
        package: "url_to_git_repo_as_go_package

    models:
        mod1:
            ...
            var1:
                plugin:
                    search:
                        name: "myplugin"
                        params: "param1,param2"
                    checks:
                        - name: "myplugin"
                          params: "param3,param4"

*search* and *checks* plugins are optional.
A plugin may define one or the other.

The *search* will use the search function of the plugin instead of logol
default one.
The *checks* adds additional controls on a match. Logol will search for
matches as usual, but it will also ask to the plugin to check its validity.
The *logol2-plugin-example* plugin for example checks that match has a minimum
percentage of "c" in found matches.

## Chaining models

At *run* it is possible to call multiple models, passing variables from one to the other:

    run:
    - model: mod1
      param:
        - R1
    - model: mod2
      param:
        - R1

This example will call mod1, and for each match of mod1 will call mod2.
Each model call can define parameters (an output param for mod1 and in input param for mod2 here).

Each model is independant, this means that mod2 may look for something at the
beginning of the sequence for example. Search will start at position 0 in any case but help
with parameters, you may apply some constraints (start position, etc...) on other models.

## Meta constraints

It is possible to apply a few constraints on main models (defined in *run*) to check that
have same length for example:

    run:
    - model: mod1
    param:
        - R1
    - model: mod2
    param:
        - R1

    meta:
    - vars.mod1.Len == vars.mod2.Len

You can use basic arithmetic function like for saved variables.
If one meta control fail, then whole match is discarded.

Meta expression are boolean expression.

To use a plugin, declare the plugin in grammar and call plugin function (see plugin documentation for parameters):

    plugins:
      - name: "myplugin"
        package: "url_to_git_repo_as_go_package

    models:
      ...
    run:
    - model: mod1
    param:
        - R1
    meta:
    - myplugin.Count(ctx, vars.R1, 'a') == 2  # count number of "a" in R1 and check count == 2

Meta can also be defined at model level:

    mod1:
    comment: 'mod1()'
    meta:
        - vars.R1.Len == 2
    start:
        - var1
    ....

However, at model level, all variables must be evaluated at model completion time.
Meta controls will be executed after full model search. If variable is not known, then match will be executed.
Reason is, due to possible branching, variable may be defined in some branches only.
In case of complex meta controls on possibly not yet evaluated variables, yse meta controls at grammar level,
and send expected variables up to the root level to be evaluated once full search is done.

## Modules

With modules, it is possible to re-use existing grammars
(as a kind of sub workflows).
Grammars may accept input parameters.

### Creating modules

A grammar can be compiled as a module using --module *modulename* option.
When compiled as a module, module must be saved in a git repository, after having executed go mod init/tidy commands in generated grammar dir:

    logol2-client --grammar mymodule.yml --output somepath
    cd somepath
    go mod init *modulename*
    go mod tidy
    git init
    git add *
    git commit -m "generate module"
    git push

The **run** property accept only 1 model when used as a model and defines the entrypoint of the grammar.

    ...
    run:
    - model: mod1
    param:
        - R1

### Using modules

To use a module, it must be defined in plugins, and in variable,
one must set *module* to true and call *modulename.Call*.

Parameter list *must* match called module expected parameters.

    plugins:
      - name: "modulename"
        package: "github.com/you/modulename"

    ...
          var3:
          comment: 'testmodule.Call(RAAA)'
          model:
            module: true
            name: 'testmodule.Call'
            param:
              - 'RAAA'
